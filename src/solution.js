const FILTER_YEAR = "2018";

// Get data by year and sort by flightnumber and payload count
const prepareData = (result) => {
    const missionByFilterYear = result.filter(
      mission => mission.launch_year === FILTER_YEAR
    );

    const nasaMissions = getNasaMissions(missionByFilterYear);
    nasaMissions.sort(function(a, b){return b.flight_number-a.flight_number});
    const sortByPayloadCount = nasaMissions.sort((a, b) =>
      b.payloads_count < a.payloads_count
        ? -1
        : Number(a.payloads_count > b.payloads_count)
    );
    return sortByPayloadCount;
};

// filter data by customer and return total number of payload
const getNasaMissions = (missions) => {
  let result = [];
  missions.forEach(mission => {
    mission.rocket.second_stage.payloads.forEach(item =>
    {
      if (
        item.customers.includes("NASA (CRS)") ||
        item.customers.includes("NASA")
      ) {
        result.push({
          flight_number: mission.flight_number,
          mission_name: mission.mission_name,
          payloads_count: item.customers.length
        });
      }
    });
  });
  return result;
};

// format json data
const renderData = (data) =>{
  const output = data.map(entry => `${JSON.stringify([entry], undefined, 2)}`);
  return document.getElementById("out").innerHTML = output;
}

module.exports = {
  prepareData: prepareData,
  renderData: renderData
};
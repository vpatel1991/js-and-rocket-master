require('babel-polyfill');
const axios = require("axios");
const { prepareData, renderData } = require("./solution");
const SPACEX_API = "https://api.spacexdata.com/v3/launches/past";

// get data from SpaceX api and render into a frontend
async function getAPIDatas() {
  const res = await axios.get(`${SPACEX_API}`);
  const data = await prepareData(res.data);
  return renderData(data);  
}
getAPIDatas();
